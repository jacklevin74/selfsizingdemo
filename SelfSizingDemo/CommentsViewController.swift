//
//  CommentsViewController.swift
//  SelfSizingDemo
//
//  Created by Jack Levin on 3/19/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import Photos

class CommentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MessageComposerViewDelegate,
BRNImagePickerSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var hotels:[String: String] = ["Test": "Yo dudes check my Glymps down here in Tahoe, don't forget to upvote", "Jack": "That Ninja Cat is totally glympsy!!", "Example Comment": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "Jack2": "That Ninja Cat is totally glympsy!!", "Jack3": "That Ninja Cat is totally glympsy!!", "Jack4": "That Ninja Cat is totally glympsy!!", "Jack5": "That Ninja Cat is totally glympsy!!", "Jack6": "That Ninja Cat is totally glympsy!!", "Jack7": "That Ninja Cat is totally glympsy!!"]
    
    var hotelNames:[String] = []
    
    var messageComposerView: MessageComposerView! = nil
    
    var tableView: UITableView!
    
    var origTableFrame: CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view, typically from a nib.
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardDidShow:"), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardDidHide:"), name: UIKeyboardDidHideNotification, object: nil)
        
        self.navigationController?.hidesBarsOnSwipe=false

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Comments"
        
        var swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "dismissKeyboard")
        
        swipe.direction = UISwipeGestureRecognizerDirection.Down
        
        self.view.addGestureRecognizer(swipe)
        
        let defaultWidth = view.frame.size.width
        let originalHeght = view.frame.size.height
        let defaultHeight = CGFloat(54.0)
        
        let tableFrame = CGRect(x: 0, y: 0, width: defaultWidth, height: originalHeght - defaultHeight )
        
        messageComposerView = MessageComposerView(keyboardOffset: 0, andMaxHeight: 50.0)
        messageComposerView.delegate = self;
    
        // Create our UITableView with our view's frame
        tableView = UITableView(frame: tableFrame)
        
        // Register our cell's class for cell reuse
        
        // Set our source and add the tableview to the view
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        self.view.addSubview(messageComposerView);
        
        
        tableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "newCell")
        
        tableView.estimatedRowHeight = 110.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        hotelNames = [String](hotels.keys)

        
    }
    
    func presentImagePickerSheet() {
        let authorization = PHPhotoLibrary.authorizationStatus()
        
        if authorization == .NotDetermined {
            PHPhotoLibrary.requestAuthorization({ (status) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.presentImagePickerSheet()
                })
            })
            
            return
        }
        
        if authorization == .Authorized {
            var sheet = BRNImagePickerSheet()
            sheet.numberOfButtons = 3
            sheet.delegate = self
            sheet.showInView(self.view)
        }
        else {
            let alertView = UIAlertView(title: NSLocalizedString("An error occurred", comment: "An error occurred"), message: NSLocalizedString("BRNImagePickerSheet needs access to the camera roll", comment: "BRNImagePickerSheet needs access to the camera roll"), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: "OK"))
            alertView.show()
        }
    }
    
    // MARK: BRNImagePickerSheetDelegate
    
    func imagePickerSheet(imagePickerSheet: BRNImagePickerSheet, titleForButtonAtIndex buttonIndex: Int) -> String {
        let photosSelected = (imagePickerSheet.numberOfSelectedPhotos > 0)
        
        if (buttonIndex == 0) {
            if photosSelected {
                return NSLocalizedString("Add comment", comment: "Add comment")
            }
            else {
                return NSLocalizedString("Take Photo Or Video", comment: "Take Photo Or Video")
            }
        }
        else {
            if photosSelected {
                return NSString.localizedStringWithFormat(NSLocalizedString("BRNImagePickerSheet.button1.Send %lu Photo", comment: "The secondary title of the image picker sheet to send the photos"), imagePickerSheet.numberOfSelectedPhotos)
            }
            else {
                return NSLocalizedString("Photo Library", comment: "Photo Library")
            }
        }
    }
    
    func imagePickerSheet(imagePickerSheet: BRNImagePickerSheet, willDismissWithButtonIndex buttonIndex: Int) {
        if buttonIndex != imagePickerSheet.cancelButtonIndex {
            if imagePickerSheet.numberOfSelectedPhotos > 0 {
                imagePickerSheet.getSelectedImagesWithCompletion({ (images) -> Void in
                    println(images)
                })
            }
            else {
                let controller = UIImagePickerController()
                controller.delegate = self
                var sourceType: UIImagePickerControllerSourceType = (buttonIndex == 2) ? .PhotoLibrary : .Camera
                if (!UIImagePickerController.isSourceTypeAvailable(sourceType)) {
                    sourceType = .PhotoLibrary
                    println("Fallback to camera roll as a source since the simulator doesn't support taking pictures")
                }
                controller.sourceType = sourceType
                
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func keyboardDidShow(notification:NSNotification?) -> Void {
        
        let s:NSValue = notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as NSValue;
        let keyboardHeight:CGFloat = s.CGRectValue().height;
        
        print(keyboardHeight);
        print(" keyboardDidShow with total cells: ");
        println(hotels.count)
        
        // store original frame
        
        origTableFrame = self.tableView.frame;

            UIView.animateWithDuration(0.5, animations: {
                
                let newTableFrame =  CGRectMake(self.origTableFrame.origin.x, self.origTableFrame.origin.y - keyboardHeight, self.origTableFrame.width, self.origTableFrame.height)
                
                self.tableView.frame = newTableFrame
                
                self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.hotels.count-1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: true);
                
            })

    }
    
    func keyboardDidHide(notification:NSNotification?) -> Void {
     
         println("keyboardDidHide");
        
        UIView.animateWithDuration(0.5, animations: {
            
           self.tableView.frame = self.origTableFrame;
            
        })
        
    }

    

    func dismissKeyboard() {
        
        println("Dismissing keyboard");
        self.messageComposerView.finishEditing();
        
    }

    
    override func viewDidAppear(animated: Bool) {
        
        //self.navigationController?.hidesBarsOnSwipe=true
        tableView.reloadData()
        
        
    }
    
    func messageComposerUserTyping () {
        println ("Typing");
    }
    
    func messageComposerFrameDidChange(frame: CGRect, duration: CGFloat) {
        println ("Stuff happening")
    }
    
    func messageComposerCameraButtonClicked() {
        
        println("camera button clicked")
        /* launch imessage like photo browser */
        dismissKeyboard();
        presentImagePickerSheet();
        
        
    }
    
    func messageComposerSendMessageClickedWithMessage(message: String!) {
        
        println(message);
        self.messageComposerView.finishEditing();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count;
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let row:Int = indexPath.row;
        var cell = tableView.dequeueReusableCellWithIdentifier("newCell", forIndexPath: indexPath) as TableViewCell
        
        
        if(row==0) {
            
            cell.myImage.image = UIImage(named:"car.jpg");
            cell.imageHeight.constant = 200.0;
            cell.nameHeight.constant = 21.0;
            cell.nameHeight.priority = 250.0;
            cell.replies.hidden = false;
            cell.share.hidden = false;
        }
            
        
        else {
            
            cell.replies.hidden = true;
            cell.share.hidden = true;
            cell.nameHeight.priority = 999.0
            cell.nameHeight.constant = 0.0;
            cell.imageHeight.constant = 0.0;
        }
        
        let hotelName = hotelNames[indexPath.row]

       
        cell.name.text = hotelName
        cell.address.text = hotels[hotelName]
        
        return cell
    }
    


}
