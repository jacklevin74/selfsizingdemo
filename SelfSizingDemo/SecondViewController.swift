//
//  TableViewController.swift
//  SelfSizingDemo
//
//  Created by Jack Levin on 3/17/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class SecondViewController: UITableViewController, TestCellDelegate, MessageComposerViewDelegate {

    var myFlag:Bool = false;
    var messageComposerView: MessageComposerView!
    
    var hotels:[String: String] = ["The Grand Del Mar": "5300 Grand Del Mar Court, San Diego, CA 92130",
        "One brave cat story": "This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave!",
        "Bardessono - Second View": "6526 Yount Street, Yountville, CA 94599",
        "Hotel Yountville - Second View": "6462 Washington Street, Yountville, CA 94599",
        "Islington Hotel": "321 Davey Street, Hobart, Tasmania 7000, Australia",
        "The Henry Jones Art Hotel": "25 Hunter Street, Hobart, Tasmania 7000, Australia",
        "Clarion Hotel City Park Grand": "22 Tamar Street, Launceston, Tasmania 7250, Australia",
        "Quality Hotel Colonial Launceston": "31 Elizabeth St, Launceston, Tasmania 7250, Australia",
        "Premier Inn Swansea Waterfront": "Waterfront Development, Langdon Rd, Swansea SA1 8PL, Wales",
        "Hatcher's Manor": "73 Prossers Road, Richmond, Clarence, Tasmania 7025, Australia"]
    
    var hotelNames:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        println ("Second table controller loaded");
        

        
        //tableView.tableFooterView?.addSubview(messageComposerView)
            
           // tableFooterView.addSubview(messageComposerView)
       
        //messageComposerView.delegate = self;
        
        
        
        tableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "newCell")
        
        hotelNames = [String](hotels.keys)
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        hotelNames = [String](hotels.keys)
        
    }
    
    
    func messageComposerSendMessageClickedWithMessage(message: String!) {
        
        println(message)
    }
    
    func eventFired() {
        
       // self.performSegueWithIdentifier("gotoSecondView", sender: self)
        
        println("Delegate fired in second view, changing flag")
        if(!myFlag) {
            myFlag = true;
        }
        else {
            myFlag = false;
        }
        tableView.reloadData()
    }
    
    func eventFiredTwo() {
         println("Second Delegate fired in second view, going to first view")
         self.performSegueWithIdentifier("firstView", sender: self)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "newCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as TableViewCell
        
        cell.delegate = self;
        
        cell.myImage.image = UIImage(named:"cat.jpg");
        
        let hotelName = hotelNames[indexPath.row]
        if (myFlag) {
            
            cell.name.text = "Second view"
            cell.address.text = "Welcome to Jack's Hotel, we are located at NorthStart near Lake Tahoe"
            cell.imageHeight.constant = 0.0;
            
        }
        else {
            
            
            cell.imageHeight.constant = 172.0;
            cell.name.text = hotelName
            cell.address.text = hotels[hotelName]
        }
        
        
        
        return cell
    }

}
