//
//  TestViewController.swift
//  SelfSizingDemo
//
//  Created by Jack Levin on 3/26/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class TestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    var hotels:[String: String] = ["Test": "Yo dudes check my Glymps down here in Tahoe, don't forget to upvote", "Jack": "That Ninja Cat is totally glympsy!!", "Example Comment": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "Jack2": "That Ninja Cat is totally glympsy!!", "Jack3": "That Ninja Cat is totally glympsy!!", "Jack4": "That Ninja Cat is totally glympsy!!", "Jack5": "That Ninja Cat is totally glympsy!!", "Jack6": "That Ninja Cat is totally glympsy!!", "Jack7": "That Ninja Cat is totally glympsy!!"]
    

    
    var tableView: UITableView!
    var hotelNames:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hidesBarsOnSwipe=false
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Activity"
        
        println ("Jack")
        
        let defaultWidth = view.frame.size.width
        let originalHeght = view.frame.size.height
        let defaultHeight = CGFloat(54.0)
        
        let tableFrame = CGRect(x: 0, y: 0, width: defaultWidth, height: originalHeght )
        
        // Create our UITableView with our view's frame
        tableView = UITableView(frame: tableFrame)
        
        // Register our cell's class for cell reuse
        
        // Set our source and add the tableview to the view
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        
        tableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "newCell")
        
        tableView.estimatedRowHeight = 110.0
        tableView.rowHeight = UITableViewAutomaticDimension

        hotelNames = [String](hotels.keys)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        //self.navigationController?.hidesBarsOnSwipe=true
        tableView.reloadData()
        
        
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let row:Int = indexPath.row;
        var cell = tableView.dequeueReusableCellWithIdentifier("newCell", forIndexPath: indexPath) as TableViewCell
        
        
        if(row==0) {
            
            cell.myImage.image = UIImage(named:"car.jpg");
            cell.imageHeight.constant = 200.0;
            cell.nameHeight.constant = 21.0;
            cell.nameHeight.priority = 250.0;
            cell.replies.hidden = false;
            cell.share.hidden = false;
        }
            
            
        else {
            
            cell.replies.hidden = true;
            cell.share.hidden = true;
            cell.nameHeight.priority = 999.0
            cell.nameHeight.constant = 0.0;
            cell.imageHeight.constant = 0.0;
        }
        
        let hotelName = hotelNames[indexPath.row]
        
        
        cell.name.text = hotelName
        cell.address.text = hotels[hotelName]
        
        return cell
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
