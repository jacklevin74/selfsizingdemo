//
//  ViewController.swift
//  SelfSizingDemo
//
//  Created by Simon Ng on 4/9/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

import UIKit



class FirstViewController: UITableViewController, TestCellDelegate  {
    
   
    
    var myFlag:Bool = false;
    
    var hotels:[String: String] = ["The Grand Del Mar": "5300 Grand Del Mar Court, San Diego, CA 92130",
                                    "One brave cat story": "This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave, This is a story about a nice Seimese cat who was very brave!",
                                    "Bardessono - First View": "6526 Yount Street, Yountville, CA 94599",
                                    "Hotel Yountville": "6462 Washington Street, Yountville, CA 94599",
                                    "Islington Hotel": "321 Davey Street, Hobart, Tasmania 7000, Australia",
                                    "The Henry Jones Art Hotel": "25 Hunter Street, Hobart, Tasmania 7000, Australia",
                                    "Clarion Hotel City Park Grand": "22 Tamar Street, Launceston, Tasmania 7250, Australia",
                                    "Quality Hotel Colonial Launceston": "31 Elizabeth St, Launceston, Tasmania 7250, Australia",
                                    "Premier Inn Swansea Waterfront": "Waterfront Development, Langdon Rd, Swansea SA1 8PL, Wales",
                                    "Hatcher's Manor": "73 Prossers Road, Richmond, Clarence, Tasmania 7025, Australia"]
    
    var hotelNames:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.performSegueWithIdentifier("testSeg", sender: self)
        
        self.navigationController?.hidesBarsOnSwipe=true
        
        println("Loading first view controller");
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.registerNib(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "newCell")
        
        hotelNames = [String](hotels.keys)

        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.title = "Glymps"

    }
    
    func eventFired() {
        
        println("Delegate fired in first view, changing flag")
        if(!myFlag) {
            myFlag = true;
        }
        else {
            myFlag = false;
        }
        tableView.reloadData()
    }
    
    
    func eventFiredTwo() {
        
        println("Second Delegate fired in first view, going to second view")
        self.performSegueWithIdentifier("gotoSecondView", sender: self)
        
    }

    override func viewDidAppear(animated: Bool) {
        self.navigationController?.hidesBarsOnSwipe=true
        
        tableView.reloadData()
    
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Get the row data for the selected row
        var rowData = indexPath.row;
        print("tapped cell ");
        println (rowData);
        self.navigationController?.hidesBarsOnSwipe=false
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.performSegueWithIdentifier("CommentView", sender: self)

    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "newCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as TableViewCell
        
        cell.myImage.image = UIImage(named:"cat.jpg");
        cell.delegate = self;
        
        let hotelName = hotelNames[indexPath.row]
        
        cell.share.hidden = true;
        
        if (myFlag) {
            
            cell.name.text = "Resized Cell, set image height to zero, first view"
            cell.address.text = "Welcome to Jack's Hotel, we are located at NorthStart near Lake Tahoe"
            cell.imageHeight.constant = 0.0;
            
        } else {

            cell.imageHeight.constant = 300.0;
            cell.name.text = hotelName
            cell.address.text = hotels[hotelName]

        }
        
        return cell
    }
}

