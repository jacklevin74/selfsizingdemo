//
//  TableViewCell.swift
//  SelfSizingDemo
//
//  Created by Jack Levin on 3/17/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

@objc protocol TestCellDelegate{
    
    func eventFired()
    func eventFiredTwo()
    
}

class TableViewCell: UITableViewCell {

    var delegate: TestCellDelegate?
    
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var share: UILabel!
    @IBOutlet weak var replies: UILabel!
    
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameHeight: NSLayoutConstraint!
    
    @IBAction func buttonPressed(sender: AnyObject) {
        println ("Button Pressed from cell");
        delegate?.eventFired()
    }
    
    @IBAction func buttonPressedTwo(sender: AnyObject) {
        println ("ButtonTwo Pressed from cell");
        delegate?.eventFiredTwo()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        println ("New Cell loading");
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
